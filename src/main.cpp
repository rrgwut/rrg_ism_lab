#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>


/*
 * @file    main.cpp
 * @author  Daniel Koguciuk <daniel.koguciuk@gmail.com>
 * @date    11.12.2014
 * @section DESCTIPTION
 *
 * This is a package specially prepared for ISM laboratory,
 * where you are suppposed to achive the following aims:
 * - general collision avoidance
 * - emergency stop with object is closer than 500mm
 * - maze escape scenario by the right hand algorithm
 */

ros::Publisher pub_velocity;
ros::Subscriber sub_laser_scan;

/*
 * @brief   Process laser scan callback.
 *
 * It's function you can use to interpret laser scan data
 * according to your task.
 */
void processLaserScan(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    //Messege definition:
    //http://docs.ros.org/api/sensor_msgs/html/msg/LaserScan.html

    ROS_INFO ("angle_min      : %f", scan->angle_min);
    ROS_INFO ("angle_max      : %f", scan->angle_max);
    ROS_INFO ("angle_increment: %f", scan->angle_increment);
    ROS_INFO ("time_increment : %f", scan->time_increment);
    ROS_INFO ("scan_time      : %f", scan->scan_time);
    ROS_INFO ("range_min      : %f", scan->range_min);
    ROS_INFO ("range_max      : %f", scan->range_max);
    ROS_INFO ("intensities_s  : %ld", scan->intensities.size());
    ROS_INFO ("ranges_s       : %ld", scan->ranges.size());

    //Messege definition:
    //http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html
    //http://docs.ros.org/api/geometry_msgs/html/msg/Vector3.html
    geometry_msgs::Twist message;
    geometry_msgs::Vector3 velocity_linear, velocity_angular;

    velocity_linear.x = 0.3;
    velocity_linear.y = 0;
    velocity_linear.z = 0;
    message.linear = velocity_linear;

    velocity_angular.x = 0;
    velocity_angular.y = 0;
    velocity_angular.z = 0;
    message.angular = velocity_angular;

    pub_velocity.publish(message);
}


/*
 * @brief   Main function.
 */
int main(int argc, char** argv)
{
    //Welcome info
    ROS_INFO("Starting rrg_ism_lab node..");

    //Init ros node
    ros::init(argc, argv, "rrg_ism_lab");

    //Ros node handle
    ros::NodeHandle n;

    //Ros rate
    ros::Rate rate(20);

    //Subscribe laser scan topic
    sub_laser_scan = n.subscribe<sensor_msgs::LaserScan>("/scan",1,processLaserScan);

    //Set publisher
    pub_velocity = n.advertise<geometry_msgs::Twist>("/cmd_vel",10);

    //While node ok
    while(n.ok())
    {
        //Sleeping
        rate.sleep();

        //Spinning callbacks
        ros::spinOnce();
    }

    return 0;
}
